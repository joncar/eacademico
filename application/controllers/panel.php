<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
                $this->load->library('html2pdf/html2pdf');
	}
       
        public function index($url = 'main',$page = 0)
	{
		$crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('CalificacionesFinales');
                $crud->set_subject('Calificaciones Finales');    
                $crud->set_relation('Materia','Materias','nombre');
                $crud->set_relation('Curso','Curso','Descripcion');
                $crud->where('NMatricula',$_SESSION['user']);
                $crud->unset_add();
                $crud->unset_edit()
                     ->unset_export()
                     ->unset_print()
                     ->unset_delete()
                     ->columns('Materia','Calificacion','Fecha','Curso','ActaN');
                $crud->callback_column('Calificacion',function($val){return $val<2?'<span style="color:red">'.$val.'</span>':'<span style="color:blue">'.$val.'</span>';});
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->title = 'Calificaciones Finales';
                $this->loadView($output);
                return $crud;
	}
        
        public function materia()
	{
		$crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('Alumnos');
                $crud->set_subject('Materias');
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->title = 'Calificaciones Finales';
                $this->loadView($output);
                return $crud;
	}
        
        public function parciales()
	{
		$crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('Parciales');               
                $crud->set_subject('Parciales');
                $crud->set_model('parciales');
                $crud->columns('Cod_Materia','Materia','Curso','TotalPuntos','PuntosCorrectos','Porcentaje','Calificacion','Ponderacion','%Asistencia');
                $crud->unset_delete()
                     ->unset_edit()
                     ->unset_add()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();
                $crud->callback_column('Porcentaje',function($val,$row){
                    return $row->TotalPuntos>0?(string)round($row->PuntosCorrectos*100/$row->TotalPuntos,2):(string)0;
                });
                $crud->callback_column('Calificacion',function($val,$row){
                    $calificacion = 0;
                    if($row->Porcentaje>=0 && $row->Porcentaje<=69)$calificacion = 1;
                    if($row->Porcentaje>69 && $row->Porcentaje<=77)$calificacion = 2;
                    if($row->Porcentaje>77 && $row->Porcentaje<=85)$calificacion = 3;
                    if($row->Porcentaje>85 && $row->Porcentaje<=93)$calificacion = 4;
                    if($row->Porcentaje>93 && $row->Porcentaje<=100)$calificacion = 5;
                    return $calificacion;
                });
                $crud->callback_column('Ponderacion',function($val,$row){
                    return $row->Calificacion*60/100;
                });
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'parciales';
                $output->title = 'Puntajes de parciales';
                $this->loadView($output);
                return $crud;
	}
        
        public function asistencia()
	{
		$crud = new ajax_grocery_CRUD();
                $crud->set_theme('flexigrid');
                $crud->set_table('n_asistencia');               
                $crud->set_subject('Asistencia');
                $crud->set_model('asistencia');
                $crud->columns('Cod_Materia','Materia','Curso','Total_Clases','Total_Asistencia','Porcentaje');
                
                $crud->callback_column('Total_Clases',function($val,$row){                    
                    return get_instance()->db->get_where('n_asistencia',array('materia'=>$row->Cod_Materia,'Anho'=>date("Y")))->num_rows;
                });
                $crud->callback_column('Total_Asistencia',function($val,$row){                    
                    get_instance()->db->select('COUNT( n_asistenciadetalle.materia_id ) AS materia');                    
                    get_instance()->db->where('nmatricula',$_SESSION['user']);
                    get_instance()->db->where('materia_id',$row->Cod_Materia);
                    get_instance()->db->group_by('materia_id');
                    return get_instance()->db->get('n_asistenciadetalle')->row(0)->materia;
                });
                $crud->callback_column('Porcentaje',function($val,$row){
                    return round($row->Total_Asistencia*100/$row->Total_Clases,2);
                });
                $crud->unset_delete()
                     ->unset_edit()
                     ->unset_add()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();                
                $output = $crud->render();
                $output->view = 'panel';
                $output->crud = 'user';
                $output->menu = 'asistencia';
                $output->title = 'Resumen de asistencia';
                $this->loadView($output);
                return $crud;
	}
        
        function update_materias(){
            foreach($this->db->get('n_asistencia')->result() as $m)
            {
                $this->db->update('n_asistenciadetalle',array('materia_id'=>$m->Materia),array('idasistencia'=>8888));
            }
        }
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
            else
            parent::loadView($crud);
        }                
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */