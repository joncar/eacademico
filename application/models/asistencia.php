<?php
/**
 * PHP grocery CRUD
 *
 * LICENSE
 *
 * Grocery CRUD is released with dual licensing, using the GPL v3 (license-gpl3.txt) and the MIT license (license-mit.txt).
 * You don't have to do anything special to choose one license or the other and you don't have to notify anyone which license you are using.
 * Please see the corresponding license file for details of these licenses.
 * You are free to use, modify and distribute this software, but all copyright information must remain.
 *
 * @package    	grocery CRUD
 * @copyright  	Copyright (c) 2010 through 2012, John Skoumbourdis
 * @license    	https://github.com/scoumbourdis/grocery-crud/blob/master/license-grocery-crud.txt
 * @version    	1.2
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 */

// ------------------------------------------------------------------------

/**
 * Grocery CRUD Model
 *
 *
 * @package    	grocery CRUD
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 * @version    	1.2
 * @link		http://www.grocerycrud.com/documentation
 */
class Asistencia  extends grocery_CRUD_Model  {

	protected $primary_key = null;
	protected $table_name = null;
	protected $relation = array();
	protected $relation_n_n = array();
	protected $primary_keys = array();

	function __construct()
    {
        parent::__construct();
    }
    
    function d()
    {
        if(!empty($_SESSION['user'])){
        $this->db->select('n_asistencia.IdAsistencia, n_asistenciadetalle.nmatricula as nmatricula, n_asistencia.Materia as Cod_Materia, Materias.nombre as Materia, Curso.descripcion as Curso');
        $this->db->join('n_asistenciadetalle','n_asistenciadetalle.idasistencia = n_asistencia.IdAsistencia','left');        
        $this->db->join('Curso','Curso.id = n_asistencia.Curso');        
        $this->db->join('Materias','Materias.id = n_asistencia.Materia');        
        $this->db->where('n_asistenciadetalle.nmatricula',$_SESSION['user']);
        $this->db->where('n_asistencia.Anho',date("Y"));
        $this->db->group_by('n_asistenciadetalle.materia_id');                
        }
    }

    function get_list()
    {    	
    	//$this->db->select($select, false);                
        $this->d();
    	$results = $this->db->get($this->table_name);                    
    	return $results->result();
    }  
    
    function get_total_results()
    {
    	//set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
    	$this->d();
    	return $this->db->get($this->table_name)->num_rows;

    }
}
