<?php
/**
 * PHP grocery CRUD
 *
 * LICENSE
 *
 * Grocery CRUD is released with dual licensing, using the GPL v3 (license-gpl3.txt) and the MIT license (license-mit.txt).
 * You don't have to do anything special to choose one license or the other and you don't have to notify anyone which license you are using.
 * Please see the corresponding license file for details of these licenses.
 * You are free to use, modify and distribute this software, but all copyright information must remain.
 *
 * @package    	grocery CRUD
 * @copyright  	Copyright (c) 2010 through 2012, John Skoumbourdis
 * @license    	https://github.com/scoumbourdis/grocery-crud/blob/master/license-grocery-crud.txt
 * @version    	1.2
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 */

// ------------------------------------------------------------------------

/**
 * Grocery CRUD Model
 *
 *
 * @package    	grocery CRUD
 * @author     	John Skoumbourdis <scoumbourdisj@gmail.com>
 * @version    	1.2
 * @link		http://www.grocerycrud.com/documentation
 */
class Parciales  extends grocery_CRUD_Model  {

	protected $primary_key = null;
	protected $table_name = null;
	protected $relation = array();
	protected $relation_n_n = array();
	protected $primary_keys = array();

	function __construct()
    {
        parent::__construct();
    }
    
    function d()
    {
        if(!empty($_SESSION['user'])){
        $this->db->select('SUM(ExamenDetalle.pc) as PuntosCorrectos, Parciales.CodExamen, Carreras.Nombre as Carrera, Curso.Descripcion as Curso, Materias.Nombre as Materia, Parciales.Materia as Cod_Materia, Parciales.Observacion,  Sum(ExamenDetalle.calif) AS SumaDecalif, Sum(Parciales.TotalPuntos) AS TotalPuntos, Parciales.Tipo_Examen, ExamenDetalle.Ausente, Parciales.Anho');
        $this->db->join('ExamenDetalle','Parciales.CodExamen = ExamenDetalle.codexamen');
        $this->db->join('Carreras','Parciales.Carrera = Carreras.Cod_Carrera');
        $this->db->join('Curso','Parciales.Curso = Curso.Cod_Curso');
        $this->db->join('Materias','Parciales.Materia = Materias.Cod_Materia');
        $this->db->group_by('Parciales.Curso, Parciales.Materia, Parciales.Observacion, ExamenDetalle.nmatricula, Parciales.Tipo_Examen, Parciales.Anho');        
        $this->db->where('Parciales.Tipo_Examen = 1 AND ExamenDetalle.nmatricula = '.$_SESSION['user'].' OR Parciales.Tipo_Examen=7 AND ExamenDetalle.Ausente=0 AND ExamenDetalle.nmatricula='.$_SESSION['user'],null,true);        
        }
    }

    function get_list()
    {    	
    	//$this->db->select($select, false);                
        $this->d();
    	$results = $this->db->get($this->table_name);                    
    	return $results->result();
    }  
    
    function get_total_results()
    {
    	//set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
    	$this->d();
    	return $this->db->get($this->table_name)->num_rows;

    }
}
