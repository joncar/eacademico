<?php

	class User extends CI_Model{
		var $log = false;

		function __construct(){
			parent::__construct();
			if(!empty($_SESSION['user']))
				$this->log = $_SESSION['user'];
		}

		function login($user,$pass)
		{
			$this->db->where('NMatricula',$user);
			$this->db->where('CedulaIdentidad',$pass);
			
			$r = $this->db->get('Alumnos');
			if($r->num_rows>0)
			{
                                $this->getParameters($r);
				return true;
			}
			else
				return false;
		}
		
		function login_short($id)
		{
			$this->getParameters($this->db->get_where('Alumnos',array('NMatricula'=>$id)));
		}
                
                function getParameters($row)
                {
                    $r = $row->row();
                    $_SESSION['user']=$r->NMatricula;                    
                    $_SESSION['nombre']=$r->Apellidos_Nombres;                    
                    $_SESSION['profesion'] = $r->Profesion;    
                    $_SESSION['carrera'] = $r->Carrera;
                }

		function unlog()
		{
			session_unset();
		}
		
		function edit($data)
		{
			$this->db->where('id',$_SESSION['user']);
			$this->db->update('user',$data);
		}

	}

?>