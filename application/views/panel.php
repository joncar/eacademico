<? $this->load->view('includes/nav') ?>
<section class="row" style="margin-right: 0px; margin-right:0px;">
    <div class="col-sm-3 col-xs-12">
        <div class="list-group">            
            <a href="<?= base_url('panel'); ?>" class="<?= empty($menu) || $menu=='panel'?'active':'' ?> list-group-item">Calificaciones Finales</a>
            <a href="<?= base_url('panel/parciales'); ?>" class="<?= !empty($menu) && $menu=='parciales'?'active':'' ?> list-group-item">Puntajes Parciales</a>
            <a href="<?= base_url('panel/asistencia'); ?>" class="<?= !empty($menu) && $menu=='asistencia'?'active':'' ?> list-group-item">Resumen de Asistencia</a>
            <a href="#" class="list-group-item">Contactos</a>            
        </div>
        <div class="row" style="margin-left:0px; margin-right:0px">
            <span style="color:red">Cualquier dudas sobre los datos académicos. Llamar al 0786 230 051</span>
        </div>
    </div>
    <div class="col-sm-9 col-xs-12" style="padding:20px; background:#f1fafa;">        
        <?php if(!empty($crud))$this->load->view('cruds/'.$crud); ?>
    </div>
</section>